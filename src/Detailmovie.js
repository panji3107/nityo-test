import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'react-bootstrap';
import globalAll from './globalFunction';

const mapStateToProps = globalAll.stateToProps;
const mapDispatchToProps = globalAll.dispatchToProps;

class Detailmovie extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movie5 : [],
    }
  }

  componentDidMount () {
    if (typeof this.props.peoples == "undefined") {
      window.location.href="/#/home";
    } else if (typeof this.props.peoples.movies == "undefined") {
      window.location.href="/#/home";
    } else {
      let movies = this.props.peoples.movies;
      if (typeof movies[4] != "undefined") {
        this.setState({
          movie5 : {
            title : movies[4].title,
            director : movies[4].director,
            producer : movies[4].producer,
            release_date : movies[4].release_date,
            opening_crawl : movies[4].opening_crawl
          }
        });
      } else {
        this.setState({
          movie5 : {
            title : "-",
            director : "-",
            producer : "-",
            release_date : "-",
            opening_crawl : "-"
          }
        });
      }
    }
  }

  render () {
    return (
      <div>
        <Container>
          <div className="wrapperInfo mt25">
            <div className="pd0 rightBlock" style={{ "width" :"60%" }}>
              <Row className="mb15 rowNoMinus">
                <Col md="8" className="pd0">
                  <h2 className="subTitleBlock">Related Another Movie's</h2>
                </Col>
                <Col md="4" className="pd0">
                  <a href="/#/home" className="textLink">Back to home</a>
                </Col>
              </Row>
              <Row className="infoBlock rowNoMinus">
                <div className="innerWrapper">
                  <div className="relatedBlock">
                    <h3 className="relatedTitle">{this.state.movie5.title}</h3>
                    <dl className="related">
                      <dt className="related" style={{ "width" :"20%" }}>Director:</dt>
                      <dd className="related" style={{ "width" :"80%" }}>{this.state.movie5.director}</dd>
                    </dl>
                    <dl className="related">
                      <dt className="related" style={{ "width" :"20%" }}>Producer:</dt>
                      <dd className="related" style={{ "width" :"80%" }}>{this.state.movie5.producer}</dd>
                    </dl>
                    <dl className="related">
                      <dt className="related" style={{ "width" :"20%" }}>Release Date:</dt>
                      <dd className="related" style={{ "width" :"80%" }}>{this.state.movie5.release_date}</dd>
                    </dl>
                  </div>
                  <div className="relatedDescriptionBlock">{this.state.movie5.opening_crawl}</div>
                </div>
              </Row>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Detailmovie);
