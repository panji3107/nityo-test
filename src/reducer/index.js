
const initState = {
	peoples: [],
};

function reducer (state = initState, action) {
	if (action.type === "GET_PEOPLES") {
		return Object.assign({},state,{
			peoples: action.payload.peoples
		});
	}
	return state;
}

export default reducer;
