import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'react-bootstrap';
import globalAll from './globalFunction';

/* Images Path */
import playerPict from './asset/images/play.png';
/* End Images Path */

const mapStateToProps = globalAll.stateToProps;
const mapDispatchToProps = globalAll.dispatchToProps;

export class Seemore extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movies4 : [],
    }
  }

  componentDidMount () {
    if (typeof this.props.peoples == "undefined") {
      window.location.href="/#/home";
    } else if (typeof this.props.peoples.movies == "undefined") {
      window.location.href="/#/home";
    } else {
      let movies = this.props.peoples.movies;
      let tmpData = [];
      for (let i = 0; i < movies.length; i++) {
        tmpData.push({
          title : movies[i].title,
          director : movies[i].director,
          release_date : movies[i].release_date
        });
      }
      this.setState({
        movies4 : tmpData
      });
    }
  }

  render () {
    return (
      <div>
        <Container>
          <div className="wrapperInfo mt25">
            <div className="pd0 leftBlock" style={{ "min-width" :"75%","max-width" :"75%", "margin-right":"0" }}>
              <Row className="mb15 rowNoMinus">
                <Col md="8" className="pd0">
                  <h2 className="subTitleBlock">Skywalker Movie's</h2>
                </Col>
                <Col md="4" className="pd0">
                  <a href="/#/home" className="textLink">Back to home</a>
                </Col>
              </Row>
              <Row className="infoBlock rowNoMinus mb50">
                {this.state.movies4.map((item,key) => (
                  <div className="innerWrapper" style={{ "margin-bottom":"5%" }} key={key}>
                    <div className={(key + 1) % 2 !== 0 ? "playerBlock backgroundPlayerPurple":"playerBlock backgroundPlayerRed"}>
                      <img src={playerPict} className="playerImg" />
                    </div>
                    <div className="playerInfoBlock">
                      <h3 className="infoTitle mb18" title="The Empire Strike">{item.title}</h3>
                      <p className="InfoDescription mb18">Director:<br />{item.director}</p>
                      <p className="InfoDescription mb0">Release:<br />{item.release_date}</p>
                    </div>
                  </div>
                ))}
              </Row>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Seemore);
