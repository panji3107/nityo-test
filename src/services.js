import request from 'superagent';

const people_endpoint = "https://swapi.co/api/people/1/";

export function getPeople () {
  return request
    .get(people_endpoint)
    .then(res => {
      let data = res.body;
      return data;
    })
    .catch(err => {
      let data = [{
        statusResponse : -1
      }];
      console.log(err);
      return data;
    });
}

export function getMovies (movie_endpoint) {
  return request
    .get(movie_endpoint);
}
