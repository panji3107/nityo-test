import { getPeoples } from './action/index';

const globalAll = {
	stateToProps : (state) => {
	  return {
	  	peoples: state.peoples,
	  }
	},
	dispatchToProps : (dispatch) => {
	  return {
	    getPeoples: (data) => dispatch(getPeoples(data)),
	  }
	}
}

export default (globalAll);
