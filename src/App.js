import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.scss';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import { getPeople,getMovies } from './services';
import globalAll from './globalFunction';

/* Images Path */
import profilePict from './asset/images/profile.png';
import playerPict from './asset/images/play.png';
/* End Images Path */

const mapStateToProps = globalAll.stateToProps;
const mapDispatchToProps = globalAll.dispatchToProps;

export class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      movies4 : [],
      movie5 : [],
      spinnerClass : "mb18",
      errorClass : "dpn"
    };

    this.setEllipsisText = this.setEllipsisText.bind(this);
    this.updateSpinner = this.updateSpinner.bind(this);
  }

  updateSpinner (status) {
    if (status) {
      this.setState({
        spinnerClass : "mb18"
      });
    } else {
      this.setState({
        spinnerClass : "mb18 dpn"
      });
    }
  }

  updateErrorStatus (status) {
    if (status) {
      this.setState({
        errorClass : ""
      });
    } else {
      this.setState({
        errorClass : "dpn"
      });
    }
  }

  componentDidMount () {
    let me = this;
    this.updateSpinner(true);
    getPeople().then( respData => { //get all data needed for UI
      if (typeof respData.name != "undefined") { //not error get data from server
        let data = {
          name : respData.name,
          lastName : respData.name.split(" ")[1],
          height : respData.height,
          mass : respData.mass,
          hair_color : respData.hair_color,
          skin_color : respData.skin_color,
          birth_year : respData.birth_year,
          gender : respData.gender,
          movies : respData.films
        };
        let moviesData = data.movies.map( async(item,key) => {
          let tmpData = await getMovies(item);
          let movie = tmpData.body;
          return {
            title : movie.title,
            director : movie.director,
            producer : movie.producer,
            release_date : movie.release_date,
            opening_crawl : movie.opening_crawl
          }
        });
        Promise.all(moviesData).then( resp => {
          data.movies = resp;
          me.props.getPeoples({ peoples : data });
        });
      } else { //error get data from server
        this.updateSpinner(false);
        this.updateErrorStatus(true);
      }
    });
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.peoples !== this.props.peoples) {
      let movies = this.props.peoples.movies;
      let tmpData = [];
      for (let i = 0; i < 4; i++) {
        tmpData.push({
          title : movies[i].title,
          director : movies[i].director,
          release_date : movies[i].release_date
        });
      }
      this.setState({
        movies4 : tmpData
      });
      if (typeof movies[4] != "undefined") {
        this.setState({
          movie5 : {
            title : movies[4].title,
            director : movies[4].director,
            producer : movies[4].producer,
            release_date : movies[4].release_date,
            opening_crawl : movies[4].opening_crawl
          }
        });
      } else {
        this.setState({
          movie5 : {
            title : "-",
            director : "-",
            producer : "-",
            release_date : "-",
            opening_crawl : "-"
          }
        });
      }
      this.updateSpinner(false);
    }
  }

  setEllipsisText (strInput) {
    let a = typeof strInput != "undefined"?strInput.split(" "):"- ".split(" ");
    let tmpStr = [];
    if (a.length > 25) {
      for (let i=0; i<26; i++) {
        tmpStr.push(a[i]);
      }
      tmpStr.push("...");
    } else {
      tmpStr = a;
    }
    return tmpStr.join(" ");
  }

  render () {

    return (
      <div>
        <Container>
          <div className="titleWrap">
            <h2 className={this.state.errorClass}>[Error, cannot get data from server]</h2>
            <Spinner animation="border" role="status" className={this.state.spinnerClass}>
              <span className="sr-only">Loading...</span>
            </Spinner>
            <h1 className="titleBlock">
              <span>{this.props.peoples.name}</span>
            </h1>
          </div>
          <div className="detailContainer">
            <div className="profilePictBlock">
              <img src={profilePict} className="profileImg" />
            </div>
            <div className="profileInfoBlock">
              <dl className="profile">
                <dt className="profile">
                  Height:
                </dt>
                <dd className="profile">
                  {this.props.peoples.height}
                </dd>
              </dl>
              <dl className="profile">
                <dt className="profile">
                  Mass:
                </dt>
                <dd className="profile">
                  {this.props.peoples.mass}
                </dd>
              </dl>
              <dl className="profile">
                <dt className="profile">
                  Hair Color:
                </dt>
                <dd className="profile">
                  {this.props.peoples.hair_color}
                </dd>
              </dl>
              <dl className="profile">
                <dt className="profile">
                  Skin Color:
                </dt>
                <dd className="profile">
                  {this.props.peoples.skin_color}
                </dd>
              </dl>
              <dl className="profile">
                <dt className="profile">
                  Birth Year:
                </dt>
                <dd className="profile">
                  {this.props.peoples.birth_year}
                </dd>
              </dl>
              <dl className="profile">
                <dt className="profile">
                  Gender:
                </dt>
                <dd className="profile">
                  {this.props.peoples.gender}
                </dd>
              </dl>
            </div>
          </div>
          <div className="wrapperInfo mb24">
            <div className="pd0 leftBlock">
              <Row className="mb15 rowNoMinus">
                <Col md="8" className="pd0">
                  <h2 className="subTitleBlock">{this.props.peoples.lastName} Movie's</h2>
                </Col>
                <Col md="4" className="pd0">
                  <a href="/#/seemore" className="textLink">See More</a>
                </Col>
              </Row>
              <Row className="infoBlock rowNoMinus mb50">
                {this.state.movies4.map((item,key) => (
                  <div className="innerWrapper" key={key}>
                    <div className={(key + 1) % 2 !== 0 ? "playerBlock backgroundPlayerPurple":"playerBlock backgroundPlayerRed"}>
                      <img src={playerPict} className="playerImg" />
                    </div>
                    <div className="playerInfoBlock">
                      <h3 className="infoTitle mb18" title="The Empire Strike">{item.title}</h3>
                      <p className="InfoDescription mb18">Director:<br />{item.director}</p>
                      <p className="InfoDescription mb0">Release:<br />{item.release_date}</p>
                    </div>
                  </div>
                ))}
              </Row>
            </div>
            <div className="pd0 rightBlock">
              <Row className="mb15 rowNoMinus">
                <Col md="12" className="pd0">
                  <h2 className="subTitleBlock">Related Another Movie's</h2>
                </Col>
              </Row>
              <Row className="infoBlock rowNoMinus">
                <div className="innerWrapper">
                  <div className="relatedBlock">
                    <h3 className="relatedTitle">{this.state.movie5.title}</h3>
                    <dl className="related">
                      <dt className="related">Director:</dt>
                      <dd className="related">{this.state.movie5.director}</dd>
                    </dl>
                    <dl className="related">
                      <dt className="related">Producer:</dt>
                      <dd className="related">{this.state.movie5.producer}</dd>
                    </dl>
                    <dl className="related">
                      <dt className="related">Release Date:</dt>
                      <dd className="related">{this.state.movie5.release_date}</dd>
                    </dl>
                  </div>
                  <div className="relatedDescriptionBlock">{this.setEllipsisText(this.state.movie5.opening_crawl)}
                  <p className="ovh relatedSeeMore"><a href="/#/detailmovie" className="textLink">See More</a></p>
                  </div>
                </div>
              </Row>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
